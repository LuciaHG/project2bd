from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession, Row
import csv

def getSparkSessionInstance(sparkConf):
    if ('sparkSessionInstance' not in globals()):
        globals()['sparkSessionInstance'] = SparkSession.builder.config(conf=sparkConf) \
                                            .enableHiveSupport().getOrCreate()
    return globals()['sparkSessionInstance']

def output():
    spark = getSparkSessionInstance(sc.getConf())        
    df = spark.sql("use default")

    #Select TOP 10 Hashtag
    df = spark.sql("select hashtag, sum(total) as suma from T_hashtag where timestamp between cast('2018-12-07 15:00:00' as timestamp) and cast('2018-12-07 16:00:00' as timestamp) group by hashtag order by suma desc limit 10")
    #Select TOP 10 keywords
    df = spark.sql("select word, sum(total) as suma from T_keyword where timestamp between cast('2018-12-07 15:00:00' as timestamp) and cast('2018-12-07 16:00:00' as timestamp) group by word order by suma desc limit 10")
    #Select TOP 10 participats
    df = spark.sql("select name, sum(total) as suma from T_screenname where timestamp between cast('2018-12-07 15:00:00' as timestamp) and cast('2018-12-08 03:00:00' as timestamp) group by name order by suma desc limit 10")
    #Select keywords between 1 HOUR DAY1
    df = spark.sql("select keyword, sum(total) as suma from T_ocurrences where timestamp between cast('2018-12-06 22:00:00' as timestamp) and cast('2018-12-06 23:00:00' as timestamp) group by keyword order by suma")
    #Select keywords between 1 HOUR DAY2
    df = spark.sql("select keyword, sum(total) as suma from T_ocurrences where timestamp between cast('2018-12-07 22:00:00' as timestamp) and cast('2018-12-07 23:00:00' as timestamp) group by keyword order by suma")
    #Select keywords between 1 HOUR DAY3
    df = spark.sql("select keyword, sum(total) as suma from T_ocurrences where timestamp between cast('2018-12-08 22:00:00' as timestamp) and cast('2018-12-08 23:00:00' as timestamp) group by keyword order by suma")

    df.show()
    df.repartition(1).write.csv("/data/result_hasshtag.csv")
    df.repartition(1).write.csv("/data/reult_users.csv")
    df.repartition(1).write.csv("/data/resuls_keywords.csv")
    df.repartition(1).write.csv("/data/ocurrencesday1.csv")
    df.repartition(1).write.csv("/data/ocurrencesday2.csv")
    df.repartition(1).write.csv("/data/ocurrencesday3.csv")
    
if __name__ == "__main__":
    sc = SparkContext(appName="Save CSV")
    output()
